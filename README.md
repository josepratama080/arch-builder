# ARCH PACKAGES BUILDER


Directly get from AUR: 
- slimjet 
- spotify
- cloudflared
- xampp


Add this to /etc/pacman.conf: 
- [arch-repository-jpratama7]
- SigLevel = Never
- Server = https://gitlab.com/josepratama080/arch-repository/-/raw/main/X86-64


Request package? Open Issue

Alternative (rarely update :v) : https://build.opensuse.org/project/show/home:jpratama7
